const YAML = require('yaml');
const fs = require('fs');

function parseDataSyncFrom(d) {
  const obj = {};
  for (const fn of fs.readdirSync(d)) {
    if (fs.statSync(d + '/' + fn).isDirectory()) {
      obj[fn] = parseDataSyncFrom(d + '/' + fn);
      continue;
    }
    const raw = fs.readFileSync(d + '/' + fn, 'utf-8');
    let content;
    if (fn.endsWith('.json')) {
      content = JSON.parse(raw);
    } else if (fn.match(/.\.ya?ml$/)) {
      content = YAML.parse(raw, {
        prettyErrors: true
      });
    } else {
      console.log("Ignore file:", fn);
      continue;
    }
    if (fn.startsWith('_') && !Array.isArray(content)) {
      Object.assign(obj, content);
    } else {
      const key = fn.replace(/\.ya?ml$/, '').replaceAll('-','_');
      obj[key] = content;
    }
  }
  return obj;
}

module.exports = {
  parseDataSyncFrom
};