(function() {
  function fillElapses() {
    const lang = document.documentElement.lang,
          experience_times = document.querySelectorAll('/* @echo config.elapses.query */');
    for (const t of experience_times) {
      const date_arr = t.dataset.range.split('/');
      if (date_arr.length > 1) {
        let d1str = date_arr[0].trim(),
              d1 = Date.parse(d1str),
              d2str = date_arr[1].trim(),
              d2,
              untilNow = d2str.match(/^-/);
        if (untilNow) {
          d2 = Date.now();
        } else {
          d2 = Date.parse(d2str);
        }
        let d2date = new Date(d2);
        d2 = Date.UTC(d2date.getUTCFullYear(), d2date.getUTCMonth() + 1 , 0, 23, 0, 0, 0);
        const diff = d2 - d1,
              days_base = 24 * 3600000,
              year_base = 365.2425 * days_base;
              years = Math.floor(diff / year_base),
              months = Math.floor((diff-(years * year_base)) / (year_base / 12));
        let text = '';
        switch (lang) {
/* @foreach $LANG in LANGUAGE_LIST */
          case '$LANG':
            if (years == 1) text+=`/* @echo locale.$LANG.time.elapses.years.one */`;
            else if (years) text+=`/* @echo locale.$LANG.time.elapses.years.multiple */`;
            if (years && months) text+="/* @echo locale.$LANG.time.elapses.conjunction */";
            if (months == 1) text+=`/* @echo locale.$LANG.time.elapses.months.one */`;
            else if (months) text+=`/* @echo locale.$LANG.time.elapses.months.multiple */`;
            break;
/* @endfor */
        }
        if (text) {
          const e = document.createElement('time');
          e.innerHTML = text;
          e.setAttribute('datetime', `P${Math.floor(diff/days_base)}D`);
          if (untilNow) e.classList.add('until-now');
          t.append(' (', e, ')');
        }
      }
    }
  }

  function onHashChange() {
    if (!window.location.hash || window.location.hash.length < 2) return;
    const query = window.location.hash + ' > details.h3',
          arr = document.querySelectorAll(query);
    for (const d of arr) {
      d.open = true;
    }
  }

  function setupShare() {
    if (navigator.share) {
      const lang = document.documentElement.lang,
            shareOpts = { url: '/* @echo config.base_dir */' },
            e = document.getElementById('share');
      // set Apple icon if (macOS or iOS) or else Google icon
      if (navigator.userAgent.match(/Mac OS X/i)) {
        e.classList.add('solid', 'fa-share-square');
      } else {
        e.classList.add('solid', 'fa-share-alt');
      }
      // set language literals
      switch (lang) {
/* @foreach $LANG in LANGUAGE_LIST */
        case '$LANG':
          shareOpts.title = "/* @echo cv.share.title.$LANG */";
          shareOpts.text = "/* @echo cv.share.text.$LANG */";
          break;
/* @endfor */
      }
      // add event listener
      e.addEventListener('click', function(e) {
        e.preventDefault();
        navigator.share(shareOpts);
        return false;
      });
      e.parentElement.classList.remove('hidden');
    }
  }

  function expandAllDetails() {
    const elements = document.querySelectorAll('details');
    for (const e of elements) {
      e.open = true;
    }
  }

  let skills_stylesheet, skills_selected = {};
  function _selectSkill(skill) {
    skills_stylesheet.appendChild(document.createTextNode('/* @echo config.skills.query */[data-skill="'+skill+'"] * {/* @echo config.skills.selected_style */}'));
  }
  function clickOnSkill(evt) {
    const skill = evt.target.parentElement.dataset.skill;
    if (skills_stylesheet) {
      if (skills_selected[skill]) {
        delete skills_selected[skill];
        clearSkills();
        Object.keys(skills_selected).map(s => _selectSkill(s));
      } else {
        skills_selected[skill] = true;
        _selectSkill(skill);
      }
    }
    expandAllDetails();
    evt.target.scrollIntoView({block: "nearest", inline: "nearest"})
  }
  function clearSkills() {
    if (skills_stylesheet) {
      skills_stylesheet.replaceChildren(document.createTextNode("")); // Hack for Safari
      skills_stylesheet.appendChild(document.createTextNode("/* @echo config.skills.query */[data-skill] * { cursor: pointer; }"));
    }
  }
  function skillsClickable() {
    skills_stylesheet = document.createElement("style");
    clearSkills(); // WebKit hack
    document.head.appendChild(skills_stylesheet);
    const elements = document.querySelectorAll('/* @echo config.skills.query */[data-skill] *');
    for (const e of elements) {
      e.addEventListener('click', clickOnSkill);
    }
  }

  window.addEventListener('load', fillElapses);
  window.addEventListener('load', setupShare);
  window.addEventListener('load', skillsClickable);
  window.addEventListener('load', onHashChange);
  window.addEventListener('hashchange', onHashChange);
  //window.addEventListener('beforeprint', expandAllDetails);
})();