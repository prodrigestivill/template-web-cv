'use strict';

const gulp = require('gulp');
const sass = require('gulp-sass')(require('sass'));
const preprocess = require('gulp-preprocess');
const minify = require('gulp-minify');
const ejs = require("gulp-ejs");
const rename = require('gulp-rename');
const htmlbeautify = require('gulp-html-beautify');
const del = require('del');
const marked = require('marked');
const { parseDataSyncFrom } = require('./src/helpers/parse-data');

const htmlbeautify_opts = {
  indent_char: '\t',
  indent_size: 1,
};
marked.setOptions({
  gfm: true,
  breaks: true,
  xhtml: false
});

let data = {}
function parseDataFolder() {
  data = parseDataSyncFrom('./data');
  data.marked = marked;
  if (process.env.NODE_ENV == 'development') {
    // force base_dir and page_suffix for development builds
    data.config.base_dir = './';
    data.config.page_suffix = '.html';
    data.config.main_page_file = true;
  }
  return Promise.resolve();
}

function clean(){
  return del('./public/**', {force:true});
}

function copyStatics() {
  return gulp.src('./static/**')
    .pipe(gulp.dest('./public/'));
}

function buildStyles(){
  return gulp.src('./src/sass/**/*.scss')
    .pipe(sass({ outputStyle: 'compressed' }).on('error', sass.logError))
    .pipe(gulp.dest('./public/assets/css'));
}

function buildScripts() {
  return gulp.src('./src/js/*.js')
    .pipe(preprocess({ context: {...data,
      LANGUAGE_LIST: data.config.languages.enabled.toString(),
    }}))
    .pipe(minify({
      noSource: true,
      ext: { min: '.js' },
    }))
    .pipe(gulp.dest('./public/assets/js'));
}

function buildMainView(){
  return gulp.src('./src/templates/main.ejs')
    .pipe(ejs(data))
    .pipe(rename({
      basename: 'index',
      extname: '.html'
    }))
    .pipe(htmlbeautify(htmlbeautify_opts))
    .pipe(gulp.dest('./public'));
}

function buildSitemap(){
  return gulp.src('./src/templates/sitemap.ejs')
    .pipe(ejs(data))
    .pipe(rename({
      basename: 'sitemap',
      extname: '.xml'
    }))
    .pipe(gulp.dest('./public'));
}

function buildLocalizedViews(){
  const subtasks = data.config.languages.enabled.map(lang => gulp
    .src('./src/templates/cv.ejs')
    .pipe(ejs({ ...data, lang }))
    .pipe(rename({
      basename: lang,
      extname: '.html'
    }))
    .pipe(htmlbeautify(htmlbeautify_opts))
    .pipe(gulp.dest('./public'))
  );
  return Promise.all(subtasks);
}

function watch(){
  return require('gulp-watch')([
    './static/**',
    './data/**',
    './src/**',
  ], {
    ignoreInitial: true,
    verbose: true,
  }, build);
}

const build = gulp.series(
  clean,
  parseDataFolder,
  copyStatics,
  gulp.parallel(
    buildStyles,
    buildScripts,
    buildMainView,
    buildLocalizedViews,
    buildSitemap,
));

const buildWatch = gulp.series(build, watch);

module.exports = {
  default: build,
  build,
  clean,
  watch,
  buildWatch,
};