# CV Web Template

This is a EJS template to generate an interactive CV.

First fill all the required information into:
 - data/config.yaml
 - data/cv/*.yaml
 - data/metadata.yaml

To generate the template while developing:
```sh
npm install
npm run build:watch
```
